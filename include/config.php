<?php
session_start();
session_regenerate_id();
error_reporting(E_ALL | E_STRICT);

// Klassen laden
include 'loader.php';
loader::load();

// Includes
require_once('../../conf.php');
require_once('datenbank.php');
require_once('functions.php');

// Klassen
function __autoload($class) {
    include 'class/' . $class . '.class.php';
}
?>