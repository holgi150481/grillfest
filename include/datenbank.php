<?php
$db = new SQL_PDO(GRILLFEST_DB_HOST, GRILLFEST_DB_USER, GRILLFEST_DB_PASS, GRILLFEST_DB_NAME);

// Tabellen
define('TABLE_ARTIKEL', 'artikel');
define('TABLE_USER', 'user');
define('TABLE_GROUPS', 'groups');

define('TABLE_TERMIN', 'termin');
define('TABLE_BESTELLUNGEN', 'bestellungen');

?>