<?php

function debug_print($var, $print = false) {
	global $debug_frame_vardump;		
	$backtrace = debug_backtrace(FALSE);
	$output =  '<pre>';
	$output .=  'Aufruf: <strong>' . $backtrace[0]['file'] . '</strong> In Zeile: <strong>'. $backtrace[0]['line'] .  '</strong><br />';
    // TODO Variablen Namen korrekt auslesen. 
	foreach ($GLOBALS as $key => $value) {
		if($value === $var) {
			$varName = $key;
		} 
	}        
	$output .= 'Variable: <strong>' . (!empty($varName) ? '&#36;' . $varName : "Unbekannt")  . '</strong><br />';
	if (is_array($var) || is_object($var)) {
		$output .= print_r($var, TRUE);	
	} else {
	    ob_start();
        var_dump($var);
		$output .= ob_get_contents();
        ob_end_clean();
	}
	$output .= '</pre>';
	echo $output;
}

function formatCurrency($float) {
	return number_format(round($float,2),2,'.','');
}

function sql2date($date) {
	if (!empty($date) && $date != "0000-00-00") {
		list($year, $month, $day) = explode("-", $date);	
		return sprintf("%02d.%02d.%04d", $day, $month, $year);
	}	
}

function date2sql($date) {
	if (!empty($date)) {
		list($day, $month, $year) = explode(".", $date);
		return sprintf("%04d-%02d-%02d", $year, $month, $day);
	}
}
    
?>