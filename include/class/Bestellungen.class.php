<?php

class Bestellungen {
		
	private $db;
	private $user_id;
	private $summe = 0;
	private $bestellungen;
	
	public function __construct($db, $user_id) {
		$this->db = $db;
		$this->user_id = $user_id;	
	}
	
	public function loadBestellungen() {
		$sql = 'SELECT * FROM ' . TABLE_BESTELLUNGEN . ' WHERE user=' . $this->user_id;
		$this->bestellungen = $this->db->query($sql);			
	}
	
	public function checkBestellung($artikel, $Nr) {		
		foreach ($this->bestellungen as $value) {
			if ($value['artikel'] == $artikel && $value['menge'] == $Nr) {
				return 'checked="checked"';
			}
		}	
	}
	
	public function getPreis($artikel, $preis) {		
		foreach ($this->bestellungen as $value) {
			if ($value['artikel'] == $artikel) {
				$ret = $value['menge'] * $preis;
				$this->summe += $ret;
				return $ret;
			} 
		}	
		return '0.00';		
	}
	
	public function updateOrder($user, $artikel, $menge) {
		$sql = 'SELECT user, artikel FROM ' . TABLE_BESTELLUNGEN . ' WHERE user=' . $user . ' AND artikel=' . $artikel . ' LIMIT 1';
		$entry = $this->db->query($sql, true);
	
		if (empty($entry)) {
			$sql = 'INSERT INTO ';
		} else { 
			$sql = 'UPDATE ';  
			$where = ' WHERE user=' . $entry['user'] . ' AND artikel=' . $entry['artikel'];
		}
		$sql .= TABLE_BESTELLUNGEN . ' SET
			user="' . $user . '",  
			artikel="' . $artikel . '",		
			menge="' . $menge .'"' .
			(!empty($where) ? $where : '');
			
		$this->db->exec($sql);		
	}
	
	public function getSumme() {
		return $this->summe;		
	}
	
}

?>