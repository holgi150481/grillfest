<?php 

if (!empty($_SESSION['login']) && $_SESSION['login'] == 'true') {
		
	$sql = 	' SELECT SUM(t1.menge) AS menge, t2.artikel ' .
			',SUM(t1.menge * t2.preis) AS betrag ' . 
			' FROM ' . TABLE_BESTELLUNGEN . ' AS t1' .
			' LEFT JOIN ' . TABLE_ARTIKEL . ' AS t2 ON t1.artikel=t2.id' .
			' GROUP BY t1.artikel';
	
	$data = $db->query($sql);
	
	$gesamtKosten = 0;
	$gesamtMenge = 0;
	foreach ($data as $value) {
		$gesamtMenge += $value['menge'];
		$gesamtKosten += $value['betrag'];
	}

} else {
	$_SESSION['error'] = 'Kein Zugriff!';
	header('location: /index.php');
	die;
}

?>