<!DOCTYPE html>
<html>
	<head>		
		<title>Einkaufsliste</title>
		<style type="text/css" media="print">
			.button {
				display: none;	
			}
		</style>
		<style type="text/css" media="screen">
			.button {
				text-align: center;
			}
		</style>
		<style>
			th {
				font-size: 1.3em;
				text-align: left;
				padding-bottom: 15px;
			}
			
			tr:first-child td {
				text-align: center;
			}
			
			td {
				font-size: 1.2em;
				padding-bottom: 10px;				
			}
			
			td:last-child {
				width: 20%;
			}
			
			tr:nth-last-child(+2) td {
				border-bottom: double;
			}
			
			tr:last-child td {
				font-weight: bold;
			}
		</style>
	</head> 
	<body>
		<div class="button">
			<button onclick="print();">Drucken</button>
			<button onclick="window.close();">Schlie&szlig;en</button>
		</div>
		<table width="70%" cellpadding="0" cellspacing="0">
			<tr>
				<td colspan="3">
					<h1>..:: Einkaufsliste ::..</h1>
				</td>
			</tr>
			<?php if (!empty($data)) { ?>
				<tr>
					<th>Artikel</th>
					<th>Menge</th>
					<th>Preis</th>
				</tr>
				<?php foreach ($data as $value) { ?>
					<tr>				
						<td><?php echo $value['artikel']; ?></td>
						<td><?php echo $value['menge']; ?></td>
						<td><?php echo formatCurrency($value['betrag']); ?> &euro;</td>						
					</tr>
				<?php } ?>
				<tr>
					<td>Gesamt:</td>
					<td><?php echo $gesamtMenge; ?></td>
					<td><?php echo formatCurrency($gesamtKosten); ?> &euro;</td>						
				</tr>
			<?php } else { ?>
				<td colspan="3">
					<h3>Keine Daten</h3>
				</td>
			<?php } ?>
		</table>
	</body>	
</html>