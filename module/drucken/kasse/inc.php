<!DOCTYPE html>
<html>
	<head>		
		<title>Kasse</title>
		<style type="text/css" media="print">
			.button {
				display: none;	
			}
		</style>
		<style type="text/css" media="screen">
			.button {
				text-align: center;
			}
		</style>
		<style type="text/css">
			table {
				width: 60%;
			}
			
			th {
				font-size: 1.3em;
				text-align: left;
				padding-bottom: 15px;
			}
			
			tr:first-child td {
				text-align: center;
			}
			
			td {
				font-size: 1.2em;
				padding-bottom: 10px;				
			}
			
			#first {
				width: 10%;
			}
			
			#sec {
				width: 70%;
			}
			
			#third {
				width: 20%;
			}
			
			tr:nth-last-child(+2) td {
				border-bottom: double;
			}
			
			tr:last-child td {
				font-weight: bold;
			}
			
			#box {
				border: 1px solid black;
				width: 20px;
				height: 20px;
				margin-left: 20px;
				margin-right: 50px;
				text-align: center;
			}
		</style>
	</head> 
	<body>
		<div class="button">
			<button onclick="print();">Drucken</button>
			<button onclick="window.close();">Schlie&szlig;en</button>
		</div>		
		<table cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td colspan="3">
					<h1>..:: Kasse ::..</h1>
				</td>
			</tr>
			<?php if (!empty($teilnehmer)) { ?>
				<?php foreach ($teilnehmer as $value) { ?>
					<tr>
						<td id="first"><div id="box"><?php echo ($value['bezahlt'] == 'true' ? 'X' : ''); ?></div></td>									
						<td id="sec"><?php echo $value['name']; ?></td>
						<td id="third"><?php echo formatCurrency($value['betrag'] + $aufrechnung); ?> &euro;</td>
					</tr>										
				<?php } ?>
				<tr>
					<td colspan="2">Gesamt:</td>
					<td><?php echo formatCurrency($gesamtKosten); ?> &euro;</td>
				</tr>
			<?php } else { ?>
				<td colspan="3">
					<h3>Keine Daten</h3>
				</td>
			<?php } ?>
		</table>
	</body>	
</html>