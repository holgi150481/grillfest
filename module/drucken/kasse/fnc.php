<?php 

if (!empty($_SESSION['login']) && $_SESSION['login'] == 'true') {
		
	$sql = 'SELECT * FROM ' . TABLE_TERMIN; 
	$termin = $db->query($sql, true);	
	
	$sql = 	'SELECT CONCAT(vorname, " ", nachname) AS name, t2.bezahlt' .
			',SUM(t1.menge * t3.preis) AS betrag ' .
			' FROM ' . TABLE_BESTELLUNGEN . ' AS t1' . 
			' LEFT JOIN ' . TABLE_USER . ' AS t2 ON t1.user=t2.id' .
			' LEFT JOIN ' . TABLE_ARTIKEL . ' AS t3 ON t1.artikel=t3.id'.
			' WHERE t1.user != "alle" AND teilnahme="true"' . 
			' GROUP BY t1.user';
	$teilnehmer = $db->query($sql);
		
	$sql = 	'SELECT SUM(t1.menge * t3.preis) AS betrag ' .
			' FROM ' . TABLE_BESTELLUNGEN . ' AS t1' . 			
			' LEFT JOIN ' . TABLE_ARTIKEL . ' AS t3 ON t1.artikel=t3.id'.
			' WHERE t1.user = "alle"';			
	$artikel_all = $db->query($sql, true);
	
	if (count($teilnehmer) > 0) {
		$aufrechnung = $artikel_all['betrag'] / count($teilnehmer);	
	} else {
		$aufrechnung = 0;
	}

	$gesamtKosten = $artikel_all['betrag'];
	foreach ($teilnehmer as $value) {		
		$gesamtKosten += $value['betrag'];
	}

} else {
	$_SESSION['error'] = 'Kein Zugriff!';
	header('location: /index.php');
	die;
}

?>