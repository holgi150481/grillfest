<?php 

if (!empty($_SESSION['login']) && $_SESSION['login'] == 'true') {
		
	function getOrder($user, $article) {
		global $db;
		$sql = 'SELECT menge FROM ' . TABLE_BESTELLUNGEN . ' WHERE user=' . $user . '  AND artikel=' . $article; 
		$data = $db->query($sql, true);	
		return $data['menge']; 
	}
		
	$sql = 'SELECT * FROM ' . TABLE_TERMIN; 
	$termin = $db->query($sql, true);	
	
	$sql = 	'SELECT artikel,id FROM ' . TABLE_ARTIKEL . ' WHERE bestellbar = "true"';
	$artikel = $db->query($sql);
	
	$sql = 	'SELECT  SUM(t2.menge) AS menge' . 
			' FROM ' . TABLE_ARTIKEL . ' AS t1' .
			' LEFT JOIN ' . TABLE_BESTELLUNGEN . ' AS t2 ON t1.id=t2.artikel' .
			' WHERE t1.bestellbar = "true"' .
			' GROUP BY t1.id';
	$artikelSum = $db->query($sql);
	
	$sql = 	'SELECT id, CONCAT(vorname, " ", nachname) AS name ' . 
			' FROM ' . TABLE_USER .	' WHERE aktiv="true" AND gruppe=' . $termin['gruppe'] . ' AND teilnahme="true"';
	$teilnehmer = $db->query($sql);
		
} else {
	$_SESSION['error'] = 'Kein Zugriff!';
	header('location: /index.php');
	die;
}

?>