<!DOCTYPE html>
<html>
	<head>		
		<title>&Uuml;bersicht</title>
		<style type="text/css" media="print">
			.button {
				display: none;	
			}
		</style>
		<style type="text/css" media="screen">
			.button {
				text-align: center;
			}
		</style>
		<style type="text/css">
			table {
				width: 100%;
			}
			
			tr:first-child td {
				text-align: center;
			}
			
			th {
				font-size: 1.3em;
				text-align: left;
				padding-bottom: 15px;
			}
			
			td {
				font-size: 1.2em;
				padding-bottom: 15px;				
			}
			
			tr:nth-last-child(+2) td {
				border-bottom: double;
			}
			
			tr:last-child td {
				font-weight: bold;
			}
			
		</style>
	</head> 
	<body>
		<div class="button">
			<button onclick="print();">Drucken</button>
			<button onclick="window.close();">Schlie&szlig;en</button>
		</div>		
		<table cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td colspan="<?php echo count($artikel) + 1; ?>">
					<h1>..:: &Uuml;bersicht ::..</h1>
				</td>
			</tr>
			<?php if (!empty($teilnehmer)) { ?>
				<tr>
					<th>Teilnehmer</th>
					<?php foreach ($artikel as $value) { ?>									
						<th><?php echo $value['artikel']; ?></th>										
					<?php } ?>
				</tr>
				<?php foreach ($teilnehmer as $value) { ?>
					<tr>
						<td><?php echo $value['name']; ?></td>
						<?php foreach ($artikel as $art) { ?>
							<td><?php echo getOrder($value['id'], $art['id']); ?></td>
						<?php } ?>
					</tr>
				<?php } ?>
				<tr>
					<td>Gesamt</td>
					<?php foreach ($artikelSum as $value) { ?>									
						<td><?php echo $value['menge']; ?></td>										
					<?php } ?>				
				</tr>
				<?php } else { ?>
				<td colspan="3">
					<h3>Keine Daten</h3>
				</td>
			<?php } ?>
		</table>
	</body>	
</html>