<!DOCTYPE html>
<html>
	<head>
		<?php require_once('header.php'); ?>
         <script>
			$(document).ready(function() {
				$('.menge').click(function() {
					var menge = parseFloat($(this).val());
					var artikel = $(this).attr('name');
					var preis = parseFloat($(this).parent().parent().parent().next().children().next().children().html());
					var gesamt = menge * preis;
					var aufpreis = parseFloat($('#aufrechnung').html());
					$(this).parent().parent().parent().next().children().next().next().next().children().html(gesamt.toFixed(2));
					var wert = 0;
					$('.zwischenwert').each(function() {
						wert = wert + parseFloat($(this).html());
					})
					$('#preis').html(wert.toFixed(2));
					$('#summe').html((wert + aufpreis).toFixed(2));
				}); 
			});
        </script>
        <style>
        	.tab_top {
        		text-align: center;
        		font-style: italic;
        		color: #708090; 
        	}
        	.tab_bot {
        		font-size: 1.7em;
        		font-weight: bold;
        		text-align: center;
        	}
        </style>  
	</head> 
	<body>
		<div class="row-fluid">
			
    		<div class="span12">
				<?php require_once('menu.php'); ?>
			</div> 
			<?php if ($termin['aktiv'] != 'true') { ?>
				<div class="span11">
    				<div class="alert alert-info" style="font-size: 1.4em; font-weight: bold; text-align: center;">Kein Event aktiv :-(</div>
    			</div>
			<?php } else { ?>
				<?php if (empty($_SESSION['user']) || (!empty($_SESSION['login']) && $_SESSION['login'] == 'true' && (!isset($_REQUEST['user']) || $_REQUEST['user'] == 'none'))) { ?>
					<form action="index.php?module=start" method="post">					
						<div class="span12 text-center">
							<div style="font-size: 1.3em; font-weight: bold;">Bitte w&auml;hlen</div>
							<br>
							<select name="user">
								<option value="none">-- Auswahl --</option>
								<?php foreach ($user_list as $login) { ?>
									<option value="<?php echo $login['id']; ?>"><?php echo $login['name']; ?></option>
								<?php } ?>
							</select>
							<br>
							<input class="btn" type="submit" name="setUser" value="Weiter" />
						</div>
					</form>
					
				<?php } else { ?>    	
					
					<div class="span11">
						<table width="100%">
							<tr>
								<td class="tab_top">Titel</td>
								<td class="tab_top">Termin</td>
								<td class="tab_top">Ort</td>
							</tr>
							<tr>
								<td class="tab_bot"><?php echo $termin['titel']; ?></td>
								<td class="tab_bot"><?php echo sql2date($termin['termin']); ?></td>
								<td class="tab_bot"><?php echo $termin['ort']; ?></td>
							</tr>
						</table>
						<br>
						<div class="span7 text-center">
							<h3><?php echo $user['name']; ?></h3>	
						</div>
					</div> 
		
					<div class="span7">
						<form action="index.php?module=start" method="post">
						<input type="hidden" name="user" value="<?php echo (!empty($_REQUEST['user']) ? $_REQUEST['user'] : ''); ?>" />			
							<?php foreach ($artikel as $value) { ?>
								<input type="hidden" name="article[]" value="<?php echo $value['id']; ?>" />
								<table class="table table-striped table-bordered">
									<tr>
										<td colspan="2" style="width: 50%"><strong><?php echo $value['artikel']; ?></strong></td>							
										<td colspan="2" style="width: 50%">
											<label class="radio inline">
												<input type="radio" class="menge" name="<?php echo $value['id']; ?>" value="0" checked="checked">
												<strong>0</strong>
											</label>
											<?php for ($i=1; $i <= $value['anzahl']; $i++) { ?> 
												<label class="radio inline">
												<input type="radio" <?php echo $bestellungen->checkBestellung($value['id'], $i); ?> class="menge" name="<?php echo $value['id']; ?>" value="<?php echo $i; ?>">
												<strong><?php echo $i; ?></strong>
											</label>
											<?php } ?>
										</td>
									</tr>
									<tr>
										<td style="width: 25%">Preis:</td>
										<td style="width: 25%"><span><?php echo formatCurrency($value['preis']); ?></span> &euro;</td>
										<td style="width: 25%">Gesamt:</td>
										<td style="width: 25%"><span class="zwischenwert"><?php echo formatCurrency($bestellungen->getPreis($value['id'], $value['preis'])); ?></span> &euro;</td>
									</tr>
								</table>
							<?php } ?>
						</div>
						<div class="span2">
							<div style="font-size: 1.2em;"><span>Preis: </span><span id="preis"><?php echo formatCurrency($bestellungen->getSumme()); ?></span><span> &euro;</span></div>
							<br>
							<div style="font-size: 1.2em;"><span>Aufpreis: </span><span id="aufrechnung"><?php echo formatCurrency($aufrechnung); ?></span><span> &euro;</span></div>
							<br>
							<div style="font-size: 1.4em;"><span>Gesamt: </span><span id="summe"><?php echo formatCurrency($bestellungen->getSumme() + $aufrechnung); ?></span><span> &euro;</span></div>
							<br>
							<input class="btn btn-success" style="width: 100px;" type="submit" name="addOrder" value="Bestellen" />
							<br>
							<br>
							Teilnahme<br>
							<select style="width: 220px;" name="teilnahme">
								<option value="true" <?php echo (!empty($user['aktiv']) && $user['teilnahme'] == 'true' ? 'selected="selected"' : ''); ?>>Ja</option>
								<option value="false" <?php echo (!empty($user['aktiv']) && $user['teilnahme'] == 'false' ? 'selected="selected"' : ''); ?>>Nein</option>								
							</select>
							<br>
							<br>
							Kommentar<br>
							<textarea style="height: 200px; width: 210px; resize: none;" name="kommentar"><?php echo (!empty($user['kommentar']) ? $user['kommentar'] : ''); ?></textarea>
						</div>
					</form>
				<?php } ?>
			<?php } ?>
    	</div>
	</body>	
</html>