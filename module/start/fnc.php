<?php
$section = 'home';

if (!empty($_REQUEST['user']) && $_REQUEST['user'] != 'none') {
	$_SESSION['user'] = $_REQUEST['user'];
} 
  
if (!empty($_SESSION['user'])) {
	
	$bestellungen = new Bestellungen($db, $_SESSION['user']);
	
	if (isset($_REQUEST['addOrder']) && !empty($_REQUEST['article'])) {
		$sql = 'UPDATE ' . TABLE_USER . ' SET teilnahme="' . $_REQUEST['teilnahme'] . '", kommentar="' . $_REQUEST['kommentar'] . '" WHERE id=' . $_SESSION['user'];		
		$db->exec($sql);		
		foreach ($_REQUEST['article'] as $value) {
			$bestellungen->updateOrder($_SESSION['user'], $value, $_REQUEST[$value]);
		}
		if ($_REQUEST['teilnahme'] == 'true') {
			$_SESSION['info'] = 'Bestellung erfolgreich gespeichert';	
		} else {			
			$sql = 'UPDATE ' . TABLE_BESTELLUNGEN . ' SET menge=0 WHERE user=' . $_SESSION['user'];		
			$db->exec($sql);			
			$_SESSION['error'] = 'Teilnahme ist nicht best&auml;tigt!';
		}		
	}
	
	$sql = 'SELECT *, CONCAT(vorname, " ", nachname) AS name FROM ' . TABLE_USER . ' WHERE id=' . $_SESSION['user'];
	$user = $db->query($sql, true);
	
	if ($user['aktiv'] == 'false') {
		unset($_SESSION['user']);
		$_SESSION['error'] = 'Fehler: Benutzer ist gesperrt!';		
	}	
	
	$bestellungen->loadBestellungen();
	
	$sql = 'SELECT * FROM ' . TABLE_ARTIKEL . ' WHERE bestellbar="true" ORDER BY artikel';
	$artikel = $db->query($sql);
	
}

$sql = 'SELECT * FROM ' . TABLE_TERMIN;
$termin = $db->query($sql, true);

$sql = 'SELECT *, CONCAT(vorname, " ", nachname) AS name FROM ' . TABLE_USER . ' WHERE gruppe=' . $termin['gruppe'] . ' AND aktiv="true" ORDER BY nachname';
$user_list = $db->query($sql);

$sql = 	'SELECT SUM(t1.menge * t3.preis) AS betrag ' .
		' FROM ' . TABLE_BESTELLUNGEN . ' AS t1' . 					
		' LEFT JOIN ' . TABLE_ARTIKEL . ' AS t3 ON t1.artikel=t3.id'.
		' WHERE t1.user = "alle"';			
$artikel_all = $db->query($sql, true);

$count = 0;
foreach ($user_list as $value) {
	if ($value['teilnahme'] == 'true') {
		$count++;
	}
}
if ($count > 0) {
	$aufrechnung = $artikel_all['betrag'] / $count;	
} else {
	$aufrechnung = $artikel_all['betrag'];
}

?>