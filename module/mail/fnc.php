<?php
$section = 'admin';

if (!empty($_SESSION['login']) && $_SESSION['login'] == 'true') {
	
	$sql = 'SELECT * FROM ' . TABLE_TERMIN; 
	$termin = $db->query($sql, true);
	
	$sql = 'UPDATE ' . TABLE_TERMIN . ' SET mailsend="true", mail_body="' . $_REQUEST['mail_body'] . '" WHERE id=' . $termin['id'];
	$db->exec($sql);
	
	$sql = 'SELECT *, CONCAT(vorname, " ", nachname) AS name FROM ' . TABLE_USER . ' WHERE aktiv="true" AND gruppe=' . $termin['gruppe']; 
	$user = $db->query($sql);
	
	
	
	if (!empty($user)) {
		foreach ($user as $value) {
			
			$message = '<strong>Grillfest Einladung</strong><br><br>';
			$message .='Hallo ' . $value['vorname'] . '<br><br>';
			$message .='Es wird herzlich zu einem neuen Grillfest geladen!<br>';
			$message .='<strong>Ort</strong>: ' . $termin['ort'] . ' <strong>Datum</strong>: ' . sql2date($termin['termin']) . '<br>';
			$message .='Bitte trag unter folgendem Link Deine Essensw&uuml;nsche ein.<br><br>';
			$message .= '<a href="http://grillen.howdynet.de?user=' . $value['id'] . '">http://grillen.howdynet.de?user=' . $value['id'] . '</a><br><br>';
			$message .= (!empty($_REQUEST['mail_body']) ? nl2br($_REQUEST['mail_body']) . '<br><br>' : '');
			$message .='Viele gr&uuml;&szlig;e ' . $termin['name'];
			$message .='';
				
			$header  = 'MIME-Version: 1.0' . "\r\n";
			$header .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			$header .= 'To: ' . $value['name'] . ' <' . $value['mail'] . '>' . "\r\n";
			$header .= 'From: ' . $termin['name'] . ' <' . $termin['mail'] . '>' . "\r\n";		
				
			mail($value['mail'], $termin['titel'] , $message, $header);
			
		}

		$_SESSION['success'] = 'Einladungen versendet!';
		header('location: /index.php?module=termin');
		die;
		
	} else {
		
		$_SESSION['error'] = 'Keine Einladung versendet!';
		header('location: /index.php?module=termin');
		die;
		
	}

} else {
	$_SESSION['error'] = 'Kein Zugriff!';
	header('location: /index.php');
	die;
}

?>