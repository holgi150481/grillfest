<!DOCTYPE html>
<html>
	<head>		
		<?php require_once('header.php'); ?>
		<script>
			var artikel = new Array ();
			<?php foreach ($artikel as $value) { ?>
				artikel[<?php echo $value['id'] ?>] =<?php echo $value['anzahl']; ?>;
			<?php } ?>			
		
			$(document).ready(function() {
				
 				$('.getKommentar').click(function() {
					$.ajax({
                		type: "post",
                		url: "index.php",   
                		data: {
                			module: 'termin/ajax',
                			art: 'get_kommentar',
                			user: $(this).attr('user')
                		},
                		success: function(response) {   
							$('#kommentarInhalt').html(response);
                    		$('#kommentar').modal('show');
                		}
					});	
				});
				
				$('.getBestellung').click(function() {
					$.ajax({
                		type: "post",
                		url: "index.php",   
                		data: {
                			module: 'termin/ajax',
                			art: 'get_bestellung',
                			user: $(this).attr('user')
                		},
                		success: function(response) {   
							$('#bestellungInhalt').html(response);
                    		$('#bestellung').modal('show');
                		}
					});	
				});
				
				$('#artikel_art').change(function() {
					var menge = $(this).val();
					$('#artikel_menge').empty();
					for (var i=0; i < artikel[menge]+1; i++) {					 	
						$("<option/>").val(i).text(i).appendTo("#artikel_menge");
					};
				});
				
			});
		</script> 
	</head> 
	<body>
		<div class="row-fluid">
			 
    		<div class="span12">
				<?php require_once('menu.php'); ?>
			</div>    		

			<div class="span11">
				<form action="index.php?module=termin" method="post">
					<input type="hidden" name="id" value="<?php echo (!empty($termin['id']) ? $termin['id'] : ''); ?>" />
							
					<div class="row-fluid">
							
						<div class="input-prepend span2">
							<span class="add-on" style="width: 80px;">Titel</span>
							<input class="input-medium" type="text" name="titel" value="<?php echo (!empty($termin['titel']) ? $termin['titel'] : ''); ?>">
						</div>
						
						<div class="input-prepend span2">
							<span class="add-on" style="width: 80px;">Ort</span>
							<input class="input-medium" type="text" name="ort" value="<?php echo (!empty($termin['ort']) ? $termin['ort'] : ''); ?>">
						</div>
						
						<div class="input-prepend span2">
							<span class="add-on" style="width: 80px;">Termin</span>
							<input class="input-medium datepicker" type="text" name="termin" value="<?php echo (!empty($termin['termin']) ? sql2date($termin['termin']) : ''); ?>">
						</div>
						
						<div class="span2">
							&nbsp;
						</div>

						<div class="input-prepend span2">
							<span class="add-on" style="width: 80px;">Gruppe</span>
							<select class="input-medium" name="gruppe">
								<option>-- Auswahl --</option>
								<?php foreach ($gruppen as $gruppe) { ?>
									<option value="<?php echo $gruppe['id']; ?>" <?php echo (!empty($termin['gruppe']) && $termin['gruppe'] == $gruppe['id']? 'selected="selected"' : ''); ?>><?php echo $gruppe['name']; ?></option>
								<?php } ?>
							</select>
						</div>						
										
					</div>	
					<div class="row-fluid">
																	
						<div class="input-prepend span2">
							<span class="add-on" style="width: 80px;">E-Mail</span>
							<input class="input-medium" type="text" name="mail" value="<?php echo (!empty($termin['mail']) ? $termin['mail'] : ''); ?>">
						</div>
						
						<div class="input-prepend span2">
							<span class="add-on" style="width: 80px;">Kontakt</span>
							<input class="input-medium" type="text" name="name" value="<?php echo (!empty($termin['name']) ? $termin['name'] : ''); ?>">
						</div>
						
						<div class="span2">
							&nbsp;
						</div>
						
						<div class="span2">
							&nbsp;
						</div>
						
						
						<div class="input-prepend span2">
							<span class="add-on" style="width: 80px;">Aktiv</span>
							<select class="input-medium" name="aktiv">
								<option value="true" <?php echo (!empty($termin['aktiv']) && $termin['aktiv'] == 'true' ? 'selected="selected"' : ''); ?>>Ja</option>
								<option value="false" <?php echo (!empty($termin['aktiv']) && $termin['aktiv'] == 'false' ? 'selected="selected"' : ''); ?>>Nein</option>								
							</select>
						</div>
						
					</div>	
					<div class="row-fluid">
						
						<div class="span2">
							<input class="btn btn-primary" type="submit" name="submit" value="Speichern">
						</div>

						<div class="span2">
							<?php $mailsend = (!empty($termin['mailsend']) && $termin['mailsend'] == 'true' ? 'btn-danger' : 'btn-success'); ?>
							<a href="#einladen" role="button" class="btn <?php echo $mailsend; ?>" data-toggle="modal">Einladen</a>
						</div>
						
						<div class="span2">
							&nbsp;
						</div>
						
						<div class="span2">
							&nbsp;
						</div>
										
						<div class="span2">
							<a href="#set_back" role="button" class="btn btn-danger" data-toggle="modal">Zur&uuml;cksetzen</a>							
						</div>
									
					</div>
				</form>    		
			</div>
			<div class="span9">
				<h4>Bestellungen</h4>
				<?php if(!empty($data)) { ?>
					<table class="table table-striped table-bordered">
						<tr>
							<th>Name</th>
							<th>Teilnahme</th>
							<th>Bezahlt</th>
							<th>Bestellung</th>
							<th>Gesamt</th>
							<th>Aktion</th>
						</tr>
						<?php foreach ($data as $value) { ?>
							<tr>
								<td><a href="index.php?module=start&user=<?php echo $value['user']; ?>"><?php echo $value['name']; ?></a></td>
								<td>
									<?php if ($value['teilnahme'] == 'true') { ?>
			    					 	<span class="badge badge-success">Ja</span> 
			    					<?php } else  { ?>
			    						<span class="badge badge-important">Nein</span> 
			    					<?php } ?>
		    					</td>
		   						<td>
									<?php if ($value['bezahlt'] == 'true') { ?>
										<a href="index.php?module=termin&bezahlt=false&user_id=<?php echo $value['user'] ?>" class="badge badge-success" title="Bezahlt"><i class="icon-ok icon-white"></i></span>								
									<?php } else { ?>
										<a href="index.php?module=termin&bezahlt=true&user_id=<?php echo $value['user'] ?>" class="badge badge-important" title="Nicht Bezahlt"><i class="icon-remove icon-white"></i></span>
									<?php }?>
		    					</td>								
								<td><?php echo formatCurrency($value['betrag']); ?> &euro;</td>
								<td><?php echo ($value['teilnahme'] == 'true' ? formatCurrency($value['betrag'] + $aufrechnung) : '0.00'); ?> &euro;</td>
								<td>
									<?php $KommentVorhanden = (!empty($value['kommentar']) && $value['kommentar'] == 'true' ? 'btn-success' : ''); ?>								
									<button class="btn btn-mini <?php echo $KommentVorhanden; ?> getKommentar" user="<?php echo $value['user']; ?>" title="Kommentar"><i class="icon-comment icon-white"></i></button>
									<button class="btn btn-mini btn-primary getBestellung" user="<?php echo $value['user']; ?>" title="Bestellungen"><i class="icon-list icon-white"></i></button>
								</td>
							</tr>
						<?php } ?>
					</table>	
				<?php } else { ?>
					<h4 style="color: #708090;">Keine Anmeldungen</h4>
				<?php } ?>
			</div>
			
			<div class="span9">
				<h4>Allgemeine Artikel</h4>
				<?php if(!empty($order_data)) { ?>
					<table class="table table-striped table-bordered">
						<tr>
							<th>Artikel</th>
							<th>Preis</th>
							<th>Menge</th>
							<th>Betrag</th>										
						</tr>
						<?php foreach ($order_data as $value) { ?>
							<tr>
								<td><?php echo $value['artikel']; ?></td>
								<td><?php echo formatCurrency($value['preis']); ?> &euro;</td>
								<td><?php echo $value['menge']; ?></td>
								<td><?php echo formatCurrency($value['betrag']); ?> &euro;</td>								
							</tr>
						<?php } ?>
					</table>	
				<?php } else { ?>
					<h4 style="color: #708090;">Keine Bestellungen</h4>
				<?php } ?>
			</div>
			
			<div class="span10">
				
				<form action="index.php?module=termin" method="post">
					<div class="row-fluid">

						<div class="input-prepend span2">
							<span class="add-on" style="width: 60px;">Artikel</span>
							<select name="artikel_art" id="artikel_art" style="width: 140px;">
								<option value="">-- Auswahl --</option>
								<?php foreach ($artikel as $art) { ?>
									<option value="<?php echo $art['id']; ?>"><?php echo $art['artikel']; ?></option>
								<?php } ?>
							</select>
						</div>
						
						<div class="input-prepend span2">
							<span class="add-on" style="width: 60px;">Menge</span>
							<select name="artikel_menge" id="artikel_menge" style="width: 140px;"></select>
						</div>					
						
						<div class="span1">
							<input class="btn btn-primary" type="submit" name="artikel_submit" value="Speichern">
						</div>
						
						<div class="span2">
							<span style="font-weight: bold;">Aufrechnung:&nbsp;<?php echo formatCurrency($aufrechnung); ?> &euro;</span>
						</div>
					
					</div>
				</form>
								
			</div>
								
    	</div>
    	    		  	  	  
		<!-- Einladen Dialog -->    	  	  
		<div id="einladen" class="modal hide fade" role="dialog" aria-hidden="true">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i></button>
				<h3>Einladung versenden</h3>
			</div>
			<form action="/index.php?module=mail" method="post" class="form-horizontal">
				<div class="modal-body">
					<?php if (!empty($termin['mailsend']) && $termin['mailsend'] == 'true') { ?>
						<h4 style="color: red;">Einladungen wurden bereits versendet!<br>Erneut senden?</h4>						
					<?php } else { ?>
						<h4>Einladungen jetzt versenden?</h4>
					<?php } ?>					
					<textarea style="width: 98%; height: 200px; resize: none;" name="mail_body"><?php echo (!empty($termin['mail_body']) ? $termin['mail_body'] : ''); ?></textarea>					
				</div>
				<div class="modal-footer">
					<input class="btn btn-primary" type="submit" value="Absenden" name="login" />
					<button class="btn" data-dismiss="modal" aria-hidden="true">Abbrechen</button>			
				</div>
			</form>
		</div>
		
		<!-- Kommentar Dialog -->   
		<div id="kommentar" class="modal hide fade" role="dialog" aria-hidden="true">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i></button>
				<h3>Kommentar</h3>
			</div>
			<div class="modal-body">
				<span id="kommentarInhalt"></span>
			</div>
			<div class="modal-footer">					
				<button class="btn btn-primary" data-dismiss="modal" aria-hidden="true">Schlie&szlig;en</button>			
			</div>
		</div>
		
		<!-- Bestellungen Dialog -->   
		<div id="bestellung" class="modal hide fade" role="dialog" aria-hidden="true">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i></button>
				<h3>Bestellung</h3>
			</div>			
			<div class="modal-body">
				<span id="bestellungInhalt"></span>
			</div>
			<div class="modal-footer">					
				<button class="btn btn-primary" data-dismiss="modal" aria-hidden="true">Schlie&szlig;en</button>			
			</div>			
		</div>
		
		<!-- History Dialog -->    	  	  
		<div id="set_back" class="modal hide fade" role="dialog" aria-hidden="true">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i></button>
				<h3>Zur&uuml;cksetzen</h3>
			</div>
			<form action="/index.php?module=termin" method="post" class="form-horizontal">
				<div class="modal-body">
					Aktuellen Termin abschlie&szlig;en und Einstellungen zur&uuml;cksetzen?					
				</div>
				<div class="modal-footer">
					<input type="submit" name="set_back" class="btn btn-danger" value="Ausf&uuml;hren" />
					<button class="btn" data-dismiss="modal" aria-hidden="true">Abbrechen</button>
				</div>
			</form>
		</div>
		
	</body>	
</html>