<?php 

if (!empty($_SESSION['login']) && $_SESSION['login'] == 'true') {
	
	if (!empty($_REQUEST['art']) && $_REQUEST['art'] == 'get_kommentar') {
		$sql = 	'SELECT kommentar FROM ' . TABLE_USER . ' WHERE id=' . $_REQUEST['user'];
		$data = $db->query($sql, true);		
		if (!empty($data['kommentar'])) {
			echo nl2br(utf8_encode($data['kommentar']));	
		} else {
			echo "Kein Kommenter hinterlassen";
		}
	}

	if (!empty($_REQUEST['art']) && $_REQUEST['art'] == 'get_bestellung') {
		$sql = 	'SELECT t2.artikel, t1.menge, (t1.menge * t2.preis) AS preis ' .
				' FROM ' . TABLE_BESTELLUNGEN . ' AS t1' .
				' LEFT JOIN ' . TABLE_ARTIKEL . ' AS t2 ON t1.artikel=t2.id' .
				' WHERE t1.menge > 0 AND user=' . $_REQUEST['user'];
		$data = $db->query($sql);
		
		$response = '<table class="table table-bordered">';
		$response .= '<tr>';
		$response .= '<th>Artikel</th>';
		$response .= '<th>Menge</th>';
		$response .= '<th>Preis</th>';
		$response .= '</tr>';
		$gesamt = 0;
		foreach ($data as $value) {
			$gesamt += $value['preis'];
			$response .= '<tr>';
			$response .= '<td>' . $value['artikel'] . '</td>';
			$response .= '<td>' . $value['menge'] . '</td>';
			$response .= '<td>' . formatCurrency($value['preis']) . ' &euro;</td>';		
			$response .= '</tr>';
		}
		$response .= '<tr>';
		$response .= '<td style="font-weight: bold;">Gesamt</td>';
		$response .= '<td>&nbsp;</td>';
		$response .= '<td style="font-weight: bold;">' . formatCurrency($gesamt) . ' &euro;</td>';
		$response .= '</tr>';		
		$response .= '<table>';
			
		echo utf8_encode($response);
	}

} else {
	$_SESSION['error'] = 'Kein Zugriff!';
	header('location: /index.php');
	die;
}