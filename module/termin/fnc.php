<?php
$section = 'termin';

if (!empty($_SESSION['login']) && $_SESSION['login'] == 'true') {
	
	if (isset($_REQUEST['bezahlt'])) {		
		$sql = 'UPDATE ' . TABLE_USER . ' SET bezahlt="' . $_REQUEST['bezahlt'] . '" WHERE id=' . $_REQUEST['user_id'];
		$db->exec($sql);
	}
	
	if (isset($_REQUEST['submit'])) {
		$sql = 'UPDATE ' . TABLE_TERMIN . ' SET
			titel="' . $_REQUEST['titel'] . '",
			ort="' . $_REQUEST['ort'] . '",    
			termin="' . date2sql($_REQUEST['termin']) . '",
			aktiv="' . $_REQUEST['aktiv'] . '",
			mail="' . $_REQUEST['mail'] . '",
			name="' . $_REQUEST['name'] . '",        
			gruppe="' . $_REQUEST['gruppe'] .'"' .
			' WHERE id=' . $_REQUEST['id'];
		$db->exec($sql);
		$_SESSION['success'] = '&Auml;nderungen &Uuml;bernommen!';
	}
	
	if (isset($_REQUEST['artikel_submit']) && !empty($_REQUEST['artikel_art'])) {		
		$sql = 'SELECT id FROM ' . TABLE_BESTELLUNGEN . ' WHERE user="alle" AND artikel=' . $_REQUEST['artikel_art'];
		$new_art = $db->query($sql, true);
		if ($_REQUEST['artikel_menge'] > 0) {
			if (empty($new_art)) {
				$sql = 'INSERT INTO ' . TABLE_BESTELLUNGEN . ' SET user="alle", artikel=' . $_REQUEST['artikel_art'] . ', menge=' . $_REQUEST['artikel_menge'];	
			} else {
				$sql = 'UPDATE ' . TABLE_BESTELLUNGEN . ' SET user="alle", artikel=' . $_REQUEST['artikel_art'] . ', menge=' . $_REQUEST['artikel_menge'] . ' WHERE id=' . $new_art['id'];
			}
			$db->exec($sql);	
		} else {
			$sql = 'DELETE FROM ' . TABLE_BESTELLUNGEN . ' WHERE id=' . $new_art['id'];
			$db->exec($sql);
		}				
	}
	
	if (isset($_REQUEST['set_back'])) {
		$sql = 'TRUNCATE TABLE ' . TABLE_BESTELLUNGEN;
		$db->exec($sql);
		
		$sql = 'UPDATE ' . TABLE_USER . ' SET teilnahme="false", kommentar=""';
		$db->exec($sql);
		
		$sql = 	'UPDATE ' . TABLE_TERMIN .' SET 
				aktiv="false", 
				termin="0000-00-00",
				gruppe="",
				titel="",
				ort="",
				mailsend="false",
				mail="",
				name="",				
				mail_body=""';
		$db->exec($sql);
		$_SESSION['info'] = 'Daten gel&ouml;scht';
	}
	
	$sql = 'SELECT * FROM ' . TABLE_TERMIN; 
	$termin = $db->query($sql, true);
	
	$sql = 'SELECT id, name FROM ' . TABLE_GROUPS;
	$gruppen = $db->query($sql);
	
	$sql = 'SELECT * FROM ' . TABLE_ARTIKEL . ' WHERE bestellbar="false"';
	$artikel = $db->query($sql);
		
	$sql = 	'SELECT t2.artikel, t1.menge, (t1.menge * t2.preis) AS betrag, t2.preis' .
			' FROM ' . TABLE_BESTELLUNGEN . ' AS t1' .
			' LEFT JOIN ' . TABLE_ARTIKEL . ' AS t2 ON t1.artikel=t2.id' . 
			' WHERE t1.user="alle"';			
	$order_data = $db->query($sql);	
	
	$sql = 	'SELECT t1.id, t2.id AS user, CONCAT(vorname, " ", nachname) AS name, t2.teilnahme' .
			',IF(t2.kommentar = "", "false", "true") AS kommentar, t2.id AS user' . 
			',SUM(t1.menge * t3.preis) AS betrag, t2.bezahlt ' .
			' FROM ' . TABLE_BESTELLUNGEN . ' AS t1' . 
			' LEFT JOIN ' . TABLE_USER . ' AS t2 ON t1.user=t2.id' .
			' LEFT JOIN ' . TABLE_ARTIKEL . ' AS t3 ON t1.artikel=t3.id' .
			' WHERE t1.user != "alle"' .
			' GROUP BY t1.user ORDER BY t2.nachname';
	$data = $db->query($sql);
	
	// Aurechnunge berechnen
	$sql = 	'SELECT SUM(t1.menge * t3.preis) AS betrag' .
		' FROM ' . TABLE_BESTELLUNGEN . ' AS t1' . 							 
		' LEFT JOIN ' . TABLE_ARTIKEL . ' AS t3 ON t1.artikel=t3.id'.
		' WHERE t1.user = "alle"';			
	$artikel_all = $db->query($sql, true);

	$count = 0;
	foreach ($data as $value) {
		if ($value['teilnahme'] == 'true') {
			$count++;
		}
	}
		
	if ($count > 0) {
		$aufrechnung = $artikel_all['betrag'] / $count;	
	} else {
		$aufrechnung = $artikel_all['betrag'];
	}
	
} else {
	$_SESSION['error'] = 'Kein Zugriff!';
	header('location: /index.php');
	die;
}

?>