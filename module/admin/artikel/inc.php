<!DOCTYPE html>
<html>
	<head>		
		<?php require_once('header.php'); ?>
	</head> 
	<body>
		<div class="row-fluid">
			
    		<div class="span12">
				<?php require_once('menu.php'); ?>
			</div>    		
			
			<div class="row-fluid">

				<form action="index.php?module=admin/artikel" method="post">
				<input type="hidden" name="id" value="<?php echo (!empty($entry['id']) ? $entry['id'] : ''); ?>" />
							
					<div class="input-prepend span2">
						<span class="add-on">Artikel</span>
						<input type="text" name="artikel" value="<?php echo (!empty($entry['artikel']) ? $entry['artikel'] : ''); ?>">
					</div>
									
					<div class="input-prepend span2">
						<span class="add-on">Anzahl</span>
						<input type="text" name="anzahl" value="<?php echo (!empty($entry['anzahl']) ? $entry['anzahl'] : ''); ?>">
					</div>
					
					<div class="input-prepend span2">
						<span class="add-on">Preis</span>
						<input type="text" name="preis" value="<?php echo (!empty($entry['preis']) ? $entry['preis'] : ''); ?>">
					</div>						
					
					<div class="input-prepend span2">
						<span class="add-on" style="width: 80px;">Bestellbar</span>
						<select class="input-medium" name="bestellbar">
							<option value="true" <?php echo (!empty($entry['bestellbar']) && $entry['bestellbar'] == 'true' ? 'selected="selected"' : ''); ?>>Ja</option>
							<option value="false" <?php echo (!empty($entry['bestellbar']) && $entry['bestellbar'] == 'false' ? 'selected="selected"' : ''); ?>>Nein</option>								
						</select>
					</div>						
					
					<div class="span2">
						<input class="btn" type="submit" name="submit" value="Speichern">
					</div>
			
				</form>
				
			</div>    		
			<div class="span10">
				<?php if(!empty($data)) { ?>
					<table class="table table-striped table-bordered">
						<tr>
							<th>Artikel</th>
							<th>Preis</th>
							<th>Anzahl</th>
							<th>Bestellbar</th>
							<th>Aktion</th>
						</tr>
						<?php foreach ($data as $value) { ?>  
							<tr>
								<td><?php echo $value['artikel']; ?></td>
								<td><?php echo formatCurrency($value['preis']); ?> &euro;</td>
								<td><?php echo $value['anzahl']; ?></td>								
								<td><?php echo ($value['bestellbar'] == 'true' ? 'Ja': 'Nein'); ?></td>
								<td>
									<a class="btn btn-mini btn-primary" href="index.php?module=admin/artikel&edit=<?php echo $value['id'] ?>" title="Berbeiten"><i class="icon-edit icon-white"></i></a>
									<a class="btn btn-mini btn-danger" href="index.php?module=admin/artikel&del=<?php echo $value['id'] ?>" title="L&ouml;schen"><i class="icon-remove icon-white"></i></a>
								</td>
							</tr>
						<?php } ?>
					</table>	
				<?php } else { ?>
					<h4>Keine Daten</h4>
				<?php } ?>
				
								
			</div>						
    	</div>
	</body>	
</html>