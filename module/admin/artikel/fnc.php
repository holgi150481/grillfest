<?php
$section = 'admin';

if (!empty($_SESSION['login']) && $_SESSION['login'] == 'true') {
	
	if (isset($_REQUEST['edit'])) {
		$sql = 'SELECT * FROM ' . TABLE_ARTIKEL . ' WHERE id = ' . $_REQUEST['edit'];
		$entry = $db->query($sql, true);
	}
		
	if (isset($_REQUEST['del'])) {
		$sql = 'DELETE FROM ' . TABLE_ARTIKEL . ' WHERE id = ' . $_REQUEST['del'];
		$db->exec($sql);
		$sql = 'DELETE FROM ' . TABLE_BESTELLUNGEN . ' WHERE artikel = ' . $_REQUEST['del'];		
		$db->exec($sql);
	}
	
	if (isset($_REQUEST['submit'])) {
		if (empty($_REQUEST['id'])) {
			$sql = 'INSERT INTO ';
		} else {
			$sql = 'DELETE FROM ' . TABLE_BESTELLUNGEN . ' WHERE user="alle" AND artikel=' . $_REQUEST['id'];
			$db->exec($sql);
			$sql = 'UPDATE ';  
			$where = ' WHERE id=' . $_REQUEST['id'];
		}
		$sql .= TABLE_ARTIKEL . ' SET
			artikel="' . $_REQUEST['artikel'] . '",
			anzahl="' . $_REQUEST['anzahl'] . '",
			bestellbar="' . $_REQUEST['bestellbar'] . '",    
			preis="' . $_REQUEST['preis'] .'"' .
			(!empty($where) ? $where : '');	
		$db->exec($sql);
	}
	
	$sql = 	'SELECT * FROM ' . TABLE_ARTIKEL . ' ORDER BY artikel';
	$data = $db->query($sql);

} else {
	$_SESSION['error'] = 'Kein Zugriff!';
	header('location: /index.php');
	die;
}
			

?>