<!DOCTYPE html>
<html>
	<head>		
		<?php require_once('header.php'); ?>
	</head> 
	<body>
		<div class="row-fluid">
			
    		<div class="span12">
				<?php require_once('menu.php'); ?>
			</div>
			
			<div class="row-fluid span10">
				<form action="index.php?module=admin/benutzer" method="post">
					<div class="input-prepend span2">
						<span class="add-on">Gruppe Filtern</span>
						<select name="gruppe_select" onchange="javascript: this.form.submit();">
							<option value="">-- Alle Anzeigen --</option>
							<?php foreach ($gruppen as $gruppe) { ?>
								<option value="<?php echo $gruppe['id']; ?>" <?php echo (!empty($_REQUEST['gruppe_select']) && $_REQUEST['gruppe_select'] == $gruppe['id']? 'selected="selected"' : ''); ?>><?php echo $gruppe['name']; ?></option>
							<?php } ?>
						</select>
					</div>
				</form>
			</div>  
			
			<div class="row-fluid span12">
				<div style="height: 10px;">&nbsp;</div>
			</div>  
			
			<div class="row-fluid">
				
				<form action="index.php?module=admin/benutzer" method="post">
				<input type="hidden" name="id" value="<?php echo (!empty($entry['id']) ? $entry['id'] : ''); ?>" />
							
					<div class="input-prepend span2">
						<span class="add-on">Vorname</span>
						<input type="text" name="vorname" value="<?php echo (!empty($entry['vorname']) ? $entry['vorname'] : ''); ?>">
					</div>
					
					<div class="input-prepend span2">
						<span class="add-on">Nachname</span>
						<input type="text" name="nachname" value="<?php echo (!empty($entry['nachname']) ? $entry['nachname'] : ''); ?>">
					</div>
					
					<div class="input-prepend span2">
						<span class="add-on">E-Mail</span>
						<input type="email" name="mail" value="<?php echo (!empty($entry['mail']) ? $entry['mail'] : ''); ?>">
					</div>
					
					<div class="input-prepend span2">
						<span class="add-on">Gruppe</span>
						<select name="gruppe">
							<option>-- Auswahl --</option>
							<?php foreach ($gruppen as $gruppe) { ?>
								<option value="<?php echo $gruppe['id']; ?>" <?php echo (!empty($entry['gruppe']) && $entry['gruppe'] == $gruppe['id']? 'selected="selected"' : ''); ?>><?php echo $gruppe['name']; ?></option>
							<?php } ?>
						</select>
					</div>
																				
					<div class="span2">
						<input class="btn" type="submit" name="submit" value="Speichern">
					</div>
				</form>
				
			</div>    		
			<div class="span11">
				<?php if(!empty($data)) { ?>
					<table class="table table-striped table-bordered">
						<tr>
							<th>Name</th>
							<th>E-Mail</th>
							<th>Gruppe</th>
							<th>Status</th>
							<th>Aktion</th>
						</tr>
						<?php foreach ($data as $value) { ?>
							<tr>
								<td><?php echo $value['name']; ?></td>
								<td><?php echo $value['mail']; ?></td>
								<td><?php echo $value['gruppe']; ?></td>
								<td>
									<?php if ($value['aktiv'] != 'false') { ?>
										<a href="index.php?module=admin/benutzer&aktiv=false&user_id=<?php echo $value['id'] ?>" class="badge badge-success" title="Aktiv"><i class="icon-ok icon-white"></i></span>								
									<?php } else { ?>
										<a href="index.php?module=admin/benutzer&aktiv=true&user_id=<?php echo $value['id'] ?>" class="badge badge-important" title="Inaktiv"><i class="icon-lock icon-white"></i></span>
									<?php }?>
								</td>								
								<td>
									<a class="btn btn-mini btn-primary" href="index.php?module=admin/benutzer&edit=<?php echo $value['id'] ?>" title="Berbeiten"><i class="icon-edit icon-white"></i></a>
									<a class="btn btn-mini btn-danger" href="index.php?module=admin/benutzer&del=<?php echo $value['id'] ?>" title="L&ouml;schen"><i class="icon-remove icon-white"></i></a>
								</td>
							</tr>
						<?php } ?>
					</table>	
				<?php } else { ?>
					<h4>Keine Daten</h4>
				<?php } ?>
				
								
			</div>						
    	</div>
	</body>	
</html>