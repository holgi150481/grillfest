<?php
$section = 'admin';

if (!empty($_SESSION['login']) && $_SESSION['login'] == 'true') {
	
	if (isset($_REQUEST['aktiv'])) {
		$sql = 'UPDATE ' . TABLE_USER . ' SET aktiv="' . $_REQUEST['aktiv'] . '" WHERE id=' . $_REQUEST['user_id'];
		$db->exec($sql);			
	}
		
	if (isset($_REQUEST['edit'])) {
		$sql = 'SELECT * FROM ' . TABLE_USER . ' WHERE id = ' . $_REQUEST['edit'];
		$entry = $db->query($sql, true);
	}
	
	if (isset($_REQUEST['del'])) {
		$sql = 'DELETE FROM ' . TABLE_USER . ' WHERE id = ' . $_REQUEST['del'];
		$db->exec($sql);	
	}
	
	if (isset($_REQUEST['submit'])) {
		if (empty($_REQUEST['id'])) {
			$sql = 'INSERT INTO ';
			$add = ', aktiv="true";';
		} else {
			$sql = 'UPDATE ';  
			$where = ' WHERE id=' . $_REQUEST['id'];
		}
		$sql .= TABLE_USER . ' SET
			vorname="' . $_REQUEST['vorname'] . '",
			nachname="' . $_REQUEST['nachname'] . '",    
			mail="' . $_REQUEST['mail'] .'",
			gruppe="' . $_REQUEST['gruppe'] .'"' .
			(!empty($add) ? $add : '') .
			(!empty($where) ? $where : '');	
		$db->exec($sql);
	}
	
	$where = '';
	if (!empty($_REQUEST['gruppe_select'])) {
		$where = ' WHERE t2.id=' . $_REQUEST['gruppe_select'];	
	}
			
	$sql =  ' SELECT t1.id, CONCAT(t1.vorname," ",t1.nachname) AS name, t1.mail, t1.aktiv, t2.name AS gruppe ' .
			' FROM ' . TABLE_USER . ' AS t1' .
			' LEFT JOIN ' . TABLE_GROUPS . ' AS t2 ON t1.gruppe=t2.id' . 
			(!empty($where) ? $where : '') .
			' ORDER BY t1.nachname';
	$data = $db->query($sql);
	
	$sql = 'SELECT id, name FROM ' . TABLE_GROUPS;
	$gruppen = $db->query($sql);
	
} else {
	$_SESSION['error'] = 'Kein Zugriff!';
	header('location: /index.php');
	die;
}

?>