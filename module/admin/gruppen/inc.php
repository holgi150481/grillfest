<!DOCTYPE html>
<html>
	<head>		
		<?php require_once('header.php'); ?>
	</head> 
	<body>
		<div class="row-fluid">
			
    		<div class="span12">
				<?php require_once('menu.php'); ?>
			</div>    		
			
			<div class="row-fluid">

				<form action="index.php?module=admin/gruppen" method="post">
				<input type="hidden" name="id" value="<?php echo (!empty($entry['id']) ? $entry['id'] : ''); ?>" />
							
					<div class="input-prepend span2">
						<span class="add-on">Name</span>
						<input type="text" name="name" value="<?php echo (!empty($entry['name']) ? $entry['name'] : ''); ?>">
					</div>
																				
					<div class="span2">
						<input class="btn" type="submit" name="submit" value="Speichern">
					</div>
			
				</form>
				
			</div>    		
			<div class="span10">
				<?php if(!empty($data)) { ?>
					<table class="table table-striped table-bordered">
						<tr>
							<th>Name</th>
							<th>Teilnehmer</th>							
							<th>Aktion</th>
						</tr>
						<?php foreach ($data as $value) { ?>  
							<tr>
								<td><?php echo $value['name']; ?></td>
								<td><?php echo getMembers($value['id']); ?></td>
								<td>
									<a class="btn btn-mini btn-primary" href="index.php?module=admin/gruppen&edit=<?php echo $value['id'] ?>" title="Berbeiten"><i class="icon-edit icon-white"></i></a>
									<a class="btn btn-mini btn-danger" href="index.php?module=admin/gruppen&del=<?php echo $value['id'] ?>" title="L&ouml;schen"><i class="icon-remove icon-white"></i></a>
								</td>
							</tr>
						<?php } ?>
					</table>	
				<?php } else { ?>
					<h4>Keine Daten</h4>
				<?php } ?>
				
								
			</div>						
    	</div>
	</body>	
</html>