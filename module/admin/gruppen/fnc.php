<?php
$section = 'admin';

if (!empty($_SESSION['login']) && $_SESSION['login'] == 'true') {
	
	function getMembers($gruppe) {
		global $db;
		$sql = 'SELECT COUNT(id) AS anzahl FROM ' . TABLE_USER . ' WHERE gruppe=' . $gruppe;
		$entry = $db->query($sql, true);
		return $entry['anzahl'];
	}	
	
	if (isset($_REQUEST['edit'])) {
		$sql = 'SELECT * FROM ' . TABLE_GROUPS . ' WHERE id = ' . $_REQUEST['edit'];
		$entry = $db->query($sql, true);
	}
	
	if (isset($_REQUEST['del'])) {
		$check_sql = 'SELECT id FROM ' . TABLE_USER . ' WHERE gruppe=' . $_REQUEST['del'];
		$check = $db->query($check_sql, true);
		if (!empty($check)) {
			$_SESSION['error'] = "<strong>Fehler:</strong> Gruppe wird noch verwendet!";	
		} else {
			$sql = 'DELETE FROM ' . TABLE_GROUPS . ' WHERE id = ' . $_REQUEST['del'];
			$db->exec($sql);
		}
	}
	
	if (isset($_REQUEST['submit'])) {
		if (empty($_REQUEST['id'])) {
			$sql = 'INSERT INTO ';
		} else {
			$sql = 'UPDATE ';  
			$where = ' WHERE id=' . $_REQUEST['id'];
		}
		$sql .= TABLE_GROUPS . ' SET			
			name="' . $_REQUEST['name'] .'"' .
			(!empty($where) ? $where : '');	
		$db->exec($sql);
	}
	
	$sql = 	'SELECT * FROM ' . TABLE_GROUPS . ' ORDER BY name';
	$data = $db->query($sql);

} else {
	$_SESSION['error'] = 'Kein Zugriff!';
	header('location: /index.php');
	die;
}

?>