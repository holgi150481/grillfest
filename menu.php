<div class="navbar">
	<div class="navbar-inner">
		<a class="brand" href="#">Grillfest</a>
		<ul class="nav">			
			<li class="<?php echo ($section == 'home' ? 'active' : ''); ?>"><a href="/index.php?module=start"><i class="icon-home"></i> Home</a></li>
			<?php if (!empty($_SESSION['login']) && $_SESSION['login'] == 'true') { ?>
				<li class="<?php echo ($section == 'termin' ? 'active' : ''); ?>"><a href="/index.php?module=termin"><i class="icon-calendar"></i> Termin</a></li>
				<li id="fat-menu" class="<?php echo ($section == 'print' ? 'active' : ''); ?> dropdown">
		        	<a href="#" id="dropdown" role="button" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-print"></i> Drucken<span class="caret"></span></a>
		            <ul class="dropdown-menu" role="menu" aria-labelledby="drop3">
		            	<li><a href="/index.php?module=drucken/uebersicht" onclick="popup(this.href); return false;">&Uuml;bersicht</a></li>
		        	   	<li><a href="/index.php?module=drucken/einkaufsliste" onclick="popup(this.href); return false;">Einkaufsliste</a></li>
		               	<li><a href="/index.php?module=drucken/kasse" onclick="popup(this.href); return false;">Kasse</a></li>
					</ul> 
				</li>
				<li id="fat-menu" class="<?php echo ($section == 'admin' ? 'active' : ''); ?> dropdown">
		        	<a href="#" id="dropdown" role="button" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-book"></i> Admin<span class="caret"></span></a>
		            <ul class="dropdown-menu" role="menu" aria-labelledby="drop3">
		        	   	<li><a href="/index.php?module=admin/benutzer">Benutzer</a></li>
		               	<li><a href="/index.php?module=admin/gruppen">Gruppen</a></li>
		               	<li><a href="/index.php?module=admin/artikel">Artikel</a></li>                	
					</ul>
				</li>
			<?php } ?>			
		</ul>
		<ul class="nav pull-right">
			<?php if (!empty($_SESSION['login']) && $_SESSION['login'] == 'true') { ?>
				<li><a href="/index.php?module=start&logout=true"><i class="icon-lock"></i> Logout</a></li>
			<?php } else { ?>
				<li class="pull-right"><a href="#modalLogin" data-toggle="modal"><i class="icon-lock"></i> Login</a></li>
			<?php } ?>		
		</ul>				
	</div>
</div>

<?php if (empty($_SESSION['login']) || $_SESSION['login'] == 'false') { ?>
	<div id="modalLogin" class="modal hide fade" role="dialog" aria-hidden="true">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i></button>
			<h3>Grillfest - Login</h3>
		</div>
		<form action="/index.php?module=<?php echo (!empty($_REQUEST['module']) ? $_REQUEST['module'] : 'start'); ?>" method="post" class="form-horizontal">			
			<div class="modal-body">
				<label for="password" style="display: inline-block;">Passwort</label>	
				<input name="password" id="password" class="input-xlarge" type="password" />
			</div>
			<div class="modal-footer">
				<input class="btn btn-primary" type="submit" value="Login" name="login" />
				<button class="btn" data-dismiss="modal" aria-hidden="true">Abbrechen</button>			
			</div>
		</form>
	</div>
<?php } ?>

<?php if (!empty($_SESSION['error'])) { ?>
	<div class="span11">
    	<div class="alert alert-error">
			<?php 
			echo $_SESSION['error'];
			unset($_SESSION['error']);				
			?>    
    	</div>
    </div>
<?php } ?>

<?php if (!empty($_SESSION['info'])) { ?>
	<div class="span11">
    	<div class="alert alert-info">
			<?php 
			echo $_SESSION['info'];
			unset($_SESSION['info']);				
			?>    
    	</div>
    </div>
<?php } ?>

<?php if (!empty($_SESSION['success'])) { ?>
	<div class="span11">
    	<div class="alert alert-success">
			<?php 
			echo $_SESSION['success'];
			unset($_SESSION['success']);				
			?>    
    	</div>
    </div>
<?php } ?>
