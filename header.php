<meta charset="ISO-8859-1" />
<title>Grillfest</title>
<link href="/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="/jquery_ui/css/smoothness/jquery-ui-1.10.3.custom.min.css" />
<script type="text/javascript" src="/jquery/jquery-1.9.1.min.js"></script>
<script src="/jquery_ui/js/jquery-ui-1.10.3.custom.min.js"></script>
<script type="text/javascript" src="/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/js/datepicker.js"></script>
<script type="text/javascript" src="/js/functions.js"></script>
