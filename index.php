<?php
require_once('include/config.php');

if (!empty($_REQUEST['password'])) {
	if ($_REQUEST['password'] == 'mampf') {
		$_SESSION['login'] = 'true';
		$_SESSION['success'] = 'Login erfolgreich';
	} else {
		$_SESSION['login'] = 'false';
		$_SESSION['error'] = 'Anmeldung fehlgeschlagen';
	}	
} 

if (!empty($_REQUEST['logout']) && $_REQUEST['logout'] == 'true') {
	$_SESSION['login'] = 'false';
	$_SESSION['info'] = 'Abgemeldet';
}

$module = (!empty($_REQUEST['module']) ? $_REQUEST['module'] : 'start'); 

if (file_exists('module/' . $module . '/fnc.php')) {
	require_once 'module/' . $module . '/fnc.php';
}

if (file_exists('module/' . $module . '/inc.php')) {
	require_once('module/' . $module . '/inc.php');
}

?>